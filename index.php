  <!DOCTYPE html>
  <html>
    <head>
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0,  user-scalable=no"/>
      <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
      <link type="text/css" rel="stylesheet" href="css/estilo.css"  media="screen,projection"/>
<style type="text/css">

body {
  font-size: 0;
  background: url(logo2.png);
  background-size: 30%;
  background-repeat: no-repeat;
  background-position: center;
  background-color: rgb(169,3,41); /* Old browsers */
  background-color: -moz-linear-gradient(45deg, rgba(169,3,41,1) 0%, rgba(143,2,34,1) 44%, rgba(109,0,25,1) 100%); /* FF3.6-15 */
  background-color: -webkit-linear-gradient(45deg, rgba(169,3,41,1) 0%,rgba(143,2,34,1) 44%,rgba(109,0,25,1) 100%); /* Chrome10-25,Safari5.1-6 */
  background-color: linear-gradient(45deg, rgba(169,3,41,1) 0%,rgba(143,2,34,1) 44%,rgba(109,0,25,1) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
}

#img {
  display: none;
}
	</style>
     
      
    </head>

    <body class="center">
    

    <img id="img" src="images/background.jpg">
   
    <div class="row container" style="position: absolute; bottom: 0; left:0; right: 0; z-index: 9999999;">
    <form class="col s12" method="post" action="index1.php">
      <div class="row">
        <div class="input-field">
          <input id="first_name" type="text" name="nome" class="validate" required style="color: #fff; text-align: center;">
          <label for="first_name" style="text-align: center;">Digite seu nome</label>
        </div>
        
      </div>
      <div class="fixed-action-btn">
	    <button type="submit" class="btn-floating btn-large yellow pulse" href="index1.php" onclick="Materialize.toast('',1000)">
	      <i class="large material-icons black-text">home</i>
	    </button>	    
	  </div>
    </form>
  	</div>
    
	  
          
        
    </body>
    <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
      <script type="text/javascript" src="js/materialize.min.js"></script>
      <script type="text/javascript" src="js/script.js"></script>
      <script type="text/javascript" src="js/typed.min.js"></script>
      <script type="text/javascript">
	var image = document.querySelector('img');
var imageCanvas = document.createElement('canvas');
var imageCanvasContext = imageCanvas.getContext('2d');
var lineCanvas = document.createElement('canvas');
var lineCanvasContext = lineCanvas.getContext('2d');
var pointLifetime = 1000;
var points = [];

if (image.complete) {
  start();
} else {
  image.onload = start;
}

/**
 * Attaches event listeners and starts the effect.
 */
function start() {
  document.addEventListener('mousemove', onMouseMove);
  window.addEventListener('resize', resizeCanvases);
  document.body.appendChild(imageCanvas);
  resizeCanvases();
  tick();
}

/**
 * Records the user's cursor position.
 *
 * @param {!MouseEvent} event
 */
function onMouseMove(event) {
  points.push({
    time: Date.now(),
    x: event.clientX,
    y: event.clientY
  });
}

/**
 * Resizes both canvases to fill the window.
 */
function resizeCanvases() {
  imageCanvas.width = lineCanvas.width = window.innerWidth;
  imageCanvas.height = lineCanvas.height = window.innerHeight;
}

/**
 * The main loop, called at ~60hz.
 */
function tick() {
  // Remove old points
  points = points.filter(function(point) {
    var age = Date.now() - point.time;
    return age < pointLifetime;
  });

  drawLineCanvas();
  drawImageCanvas();
  requestAnimationFrame(tick);
}

/**
 * Draws a line using the recorded cursor positions.
 *
 * This line is used to mask the original image.
 */
function drawLineCanvas() {
  var minimumLineWidth = 25;
  var maximumLineWidth = 100;
  var lineWidthRange = maximumLineWidth - minimumLineWidth;
  var maximumSpeed = 50;

  lineCanvasContext.clearRect(0, 0, lineCanvas.width, lineCanvas.height);
  lineCanvasContext.lineCap = 'round';
  lineCanvasContext.shadowBlur = 30;
  lineCanvasContext.shadowColor = '#000';
  
  for (var i = 1; i < points.length; i++) {
    var point = points[i];
    var previousPoint = points[i - 1];

    // Change line width based on speed
    var distance = getDistanceBetween(point, previousPoint);
    var speed = Math.max(0, Math.min(maximumSpeed, distance));
    var percentageLineWidth = (maximumSpeed - speed) / maximumSpeed;
    lineCanvasContext.lineWidth = minimumLineWidth + percentageLineWidth * lineWidthRange;

    // Fade points as they age
    var age = Date.now() - point.time;
    var opacity = (pointLifetime - age) / pointLifetime;
    lineCanvasContext.strokeStyle = 'rgba(0, 0, 0, ' + opacity + ')';
    
    lineCanvasContext.beginPath();
    lineCanvasContext.moveTo(previousPoint.x, previousPoint.y);
    lineCanvasContext.lineTo(point.x, point.y);
    lineCanvasContext.stroke();
  }
}

/**
 * @param {{x: number, y: number}} a
 * @param {{x: number, y: number}} b
 * @return {number} The distance between points a and b
 */
function getDistanceBetween(a, b) {
  return Math.sqrt(Math.pow(a.x - b.x, 2) + Math.pow(a.y - b.y, 2));
}

/**
 * Draws the original image, masked by the line drawn in drawLineToCanvas.
 */
function drawImageCanvas() {
  // Emulate background-size: cover
  var width = imageCanvas.width;
  var height = imageCanvas.width / image.naturalWidth * image.naturalHeight;
  
  if (height < imageCanvas.height) {
    width = imageCanvas.height / image.naturalHeight * image.naturalWidth;
    height = imageCanvas.height;
  }

  imageCanvasContext.clearRect(0, 0, imageCanvas.width, imageCanvas.height);
  imageCanvasContext.globalCompositeOperation = 'source-over';
  imageCanvasContext.drawImage(image, 0, 0, width, height);
  imageCanvasContext.globalCompositeOperation = 'destination-in';
  imageCanvasContext.drawImage(lineCanvas, 0, 0);
}
</script>

      <script type="text/javascript">
        // Gerar Indices 
        var i = null;
        const livros = [];
        $('.corpo tr').each(function(){
          var id = $(this).find('.id');
          var titulo = $(this).find('.titulo').text();

          livros.push({"titulo": titulo});
        });
        // Modal
        $(document).ready(function(){
          $('.modal').modal();
           $(".owl-carousel").owlCarousel();
        });
        // Compra parte 1
        function comprar(e) {
          $('.nId').val(e);
          $('.nProd').text(livros[e-1]['titulo']);
          $('#modal1').modal('open');
        }
        // Pesquisar ou Autocomplete
      </script>
  </html>