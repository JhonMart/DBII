<?php

include_once("./modelo/livro.php");
include_once("./controle/livroControle.php");
include_once("./uteis/uteis.php");

echo ' 
<br>
<div class="center">

  <br>
  <h5 class="white-text write"></h5>
  </div>
  ';

$controle = new LivroControle();
$livros = $controle->consultarLivros();

?>

<table class="bordered white highlight" align="center" style="margin-top: -80px;">
<thead id="tblPersonalizada" class="blue-grey darken-4 white-text">
	<th><i class="material-icons left">fingerprint</i>Id</th>
	<th><i class="material-icons left">title</i>Título</th>
	<th><i class="material-icons left">attach_money</i>Preço</th>
	
	<th><i class="material-icons left">attach_money</i>Comprar</th>
</thead>
<tbody class="corpo">
	<?php
		foreach($livros as $livro) {
			imprimirLivroLinha($livro);
		}
		
	?>
</tbody>
</table>

<?php

$controle = null;
$livros = null;
?>