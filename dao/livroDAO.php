<?php

include_once("./modelo/livro.php");
// Biblioteca Firebase

	require 'lib\src\firebaseLib.php';
	const DEFAULT_URL = 'https://estudos-faculdade.firebaseio.com'; 
	const DEFAULT_TOKEN = 'IiOWFgJXRi64N6hXCLx6d0xtDYGlJT5Fuh2FTf2U'; 
	const DEFAULT_PATH = '/estudos-faculdade/';
	// Fim

class LivroDAO{
	// construtor
	public function __construct(){

	}

	/************************************************************************/
	/* Recupera todos os livros cadastrados na tabela livro									*/
	/************************************************************************/
	public function consultarLivros(){
		$firebase = new Firebase\FirebaseLib(DEFAULT_URL, DEFAULT_TOKEN);

		$resultado = json_decode($firebase->get("/Livraria/livros/"),true);
		$livros = null;

		foreach ($resultado as $linha){
			$id     = $linha['id'];
		  	$titulo = $linha['titulo'];
		  	$preco  = $linha['preco'];
		  	$edicao = $linha['edicao'];

			$livro = new Livro($id, $titulo, $preco, $edicao, 0);
			$livros[] = $livro;
		}

		$resultado = null;
		$pdo = null;

		return $livros;
	}

	/************************************************************************/
	/* Consulta informações referente ao livro corresponde ao ID informado  */
	/************************************************************************/
	public function consultarLivrobyId($id) {
		$firebase = new Firebase\FirebaseLib(DEFAULT_URL, DEFAULT_TOKEN);

		$resultado = json_decode($firebase->get("/Livraria/livros/".($id-1)."/"),true);
		$livro = null;

		$id       	= $resultado['id'];
		$titulo   	= $resultado['titulo'];
		$preco    	= $resultado['preco'];
		$edicao   	= $resultado['edicao'];
		$quantidade = $resultado['quantidade'];

		$livro = new Livro($id, $titulo, $preco, $edicao, $quantidade);

		$resultado = null;
		$pdo = null;

		return $livro;
	}

} // fim da classe

?>
