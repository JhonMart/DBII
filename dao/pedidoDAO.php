<?php

include_once("./modelo/livro.php");
include_once("./modelo/pedido.php");
include_once("./modelo/item.php");

class PedidoDAO{

	// construtor
	public function __construct(){

	}

  /************************************************************************/
  /* Recupera todos os livros cadastrados na tabela livro									*/
  /************************************************************************/
  public function inserir($pedido){

    $item = $pedido->getItem();
    $idproduto = $item->getId();
    $quantidade = $item->getQuantidade();
    /* 
      Criar na tabela de pedidos
      Inserir na tabela de itempedido
      Diminuir Quantidade do Estoque
    */
    $firebase = new Firebase\FirebaseLib(DEFAULT_URL, DEFAULT_TOKEN);
    $idP = count(json_decode($firebase->get("/Livraria/pedido/"),true));
    $idI = count(json_decode($firebase->get("/Livraria/itempedido/"),true));
    $date = json_decode($firebase->get("/Livraria/livros/".($item->getId()-1)."/"),true);

    $pedidoC = array(
      "idpedido"=>($idP+1),
      "user" => $pedido->getUser()
    );

    $idAtual = ($item->getId()-1);
    $atual = $date['quantidade']-$item->getQuantidade();

    $item = array(
      "idpedido"=>($idP+1),
      "idlivro"=>$item->getId(),
      "quantidade"=>$item->getQuantidade()
    );
    if($atual >= 0){
      $firebase->set("/Livraria/livros/$idAtual/quantidade/", $atual);
      $firebase->set("/Livraria/pedido/".$idP, $pedidoC);
      $firebase->set("/Livraria/itempedido/".$idI,$item);
      $ultPedido = json_decode($firebase->get("/Livraria/pedido/$idP/"),true);

      $idU = $pedido->getUser();
      if($ultPedido['user']==$idU) {
        echo "Estoque atualizado";
        return "success";
      }else{
         echo "Não foi dessa vez";
         return "fail";
      }
    }
  }
}
?>