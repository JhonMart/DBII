<?php

include_once("./modelo/livro.php");

function imprimirLivroLinha($livro){
    $id = $livro->getId();
    echo "<tr>
            <td class='id'>". $id ."</td>
            <td class='titulo'>". $livro->getTitulo() ."</td>
            <td class='preco'>R$ ". $livro->getPreco() ."</td>
            
            
            <td>
                <a class='btn btn-floating btn-large blue' onclick='comprar(".$id.")'><i class='material-icons left'>check_circle</i></a>
            </td>
          </tr>";
}

function imprimirLivroDetalhado($livro){
    echo "<ul>
            <li class='id'>ID: " . $livro->getId() . "</li>
            <li class='titulo'>Titulo: " . $livro->getTitulo() . "</li>
            <li>Preço: " . $livro->getPreco() . "</li>
            <li> Quantidade: " . $livro->getQuantidade() . "</li>
          </ul>";
}
?>