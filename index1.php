  <!DOCTYPE html>
  <html>
    <head>
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
      <link type="text/css" rel="stylesheet" href="css/owl.carousel.min.css"  media="screen,projection"/>
      <link type="text/css" rel="stylesheet" href="css/estilo.css"  media="screen,projection"/>

      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
      <script type="text/javascript" src="https://cdn.bootcss.com/typed.js/1.1.4/typed.min.js"></script>
      <style type="text/css">
          #logo{
              width: 100px;
              transition-duration: .5s;
          }
          #logo:hover{
              filter: grayscale(1);
              transition-duration: .5s;
              margin-top:50px;
              transform: scale(3,3) rotate(-10deg);
              
              
          }
          #animation{
            transition-duration: .5s;
            
          }
          #animation:hover{
            transition-duration: .5s;
            transform: scale(1.2,1.2) rotate(-365deg);
          }
      </style>
      <!--Let browser know website is optimized for mobile-->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no"/>
    </head>

    <body>
    <div class="background"></div>
     <nav class="transparent z-depth-0">
    <div class="nav-wrapper container">
      <a href="#!" class="brand-logo"><img id="logo" class="responsive-img" src="images/logo2.png"></a>
      <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
      <ul class="right hide-on-med-and-down">
        <li><a href="index.php"><i class="material-icons left">home</i>Início</a></li>
        <li><a href="index1.php"><i class="material-icons left">monetization_on</i>Comprar</a></li>
        <li><a href="cadastro.php"><i class="material-icons left">library_books</i>Cadastrar Livro</a></li>
        <li><a href="sobre.php"><i class="material-icons left">info</i>Sobre</a></li>
        
      </ul>
      <ul class="side-nav" id="mobile-demo">
        <li><a href="index.php"><i class="material-icons left">home</i>Início</a></li>
        <li><a href="index1.php"><i class="material-icons left">monetization_on</i>Comprar</a></li>
        <li><a href="cadastro.php"><i class="material-icons left">library_books</i>Cadastrar Livro</a></li>
        <li><a href="sobre.php"><i class="material-icons left">info</i>Sobre</a></li>
      </ul>
    </div>
    
  </nav>
  
  <div class="center container" style="margin-top: 110px;">
  
  <div id="animation" class="animation owl-carousel">
    <div class="item card" >
        <div class="card-content black-text">
          <?php
            $nome = isset($_POST['nome'])? $_POST['nome']: 'Cliente';
            echo '<h5>Olá <b><i class="yellow-text">'.$nome.'</i></b></h5>'; 
          ?>
        <p>Seja bem vindo a nossa livraria, fique a vontade!</p>
        </div>
     </div>

     <div class="item card">
        <div class="card-content black-text">
        <h5>Promoção</h5>
        <p>Mês de <b class="yellow-text" style="text-transform: uppercase;">Outubro</b> com 30% de desconto</p>
        </div>
     </div>

     <div class="item card" >
        <div class="card-content black-text">
        <h5>7 Dicas de Leitura</h5>
        <p>Administrar o tempo de leitura é tão importante (e difícil) quanto controlar as finanças pessoais.</p>
        </div>
     </div>

     <div class="item card yellow">
        <div class="card-content black-text">
        <h5>Dica 1:</h5>
        <p>Tenha sempre um livro ao seu alcance</p>
        </div>
     </div>

     <div class="item card green">
        <div class="card-content black-text">
        <h5>Dica 2:</h5>
        <p>Aceite um desafio</p>
        </div>
     </div>
     <div class="item card blue">
        <div class="card-content black-text">
        <h5>Dica 3:</h5>
        <p>Marque um compromisso</p>
        </div>
     </div>
     <div class="item card red accent-3">
        <div class="card-content black-text">
        <h5>Dica 4:</h5>
        <p>Elimine as distrações</p>
        </div>
     </div>

      <div class="item card cyan">
        <div class="card-content black-text">
        <h5>Dica 5:</h5>
        <p>Varie para não enjoar </p>
        </div>
     </div>

     <div class="item card pink accent-2">
        <div class="card-content black-text">
        <h5>Dica 6:</h5>
        <p>Crie um diário de leituras </p>
        </div>
     </div>
     <div class="item card orange">
        <div class="card-content black-text">
        <h5>Dica 7:</h5>
        <p>Compartilhe suas experiências </p>
        </div>
     </div>

  </div>  

	 
	</div>
	<?php 
        include 'listaLivros.php';  
  ?>
	<div class="sombra"></div>

	</div>
	<div class="fixed-action-btn toolbar">
    <a class="btn-floating btn-large red">
      <i class="large material-icons">mode_edit</i>
    </a>
    <ul>
      <li class="waves-effect waves-light"><a href="./cadastro.php"><i class="material-icons">library_add</i></a></li>
      <li class="waves-effect waves-light"><a href="#!"><i class="material-icons">format_quote</i></a></li>
      <li class="waves-effect waves-light"><a href="#!"><i class="material-icons">publish</i></a></li>
      <li class="waves-effect waves-light"><a href="#!"><i class="material-icons">attach_file</i></a></li>
    </ul>
  </div>
 
    <!-- Modal Structure -->
    <div id="modal1" class="modal">
      <div class="modal-content">
        <h4 class="nProd">Nome do Produto</h4>
        <form action="compraLivro02.php" method="POST">
          <input type="hidden" name="user" value="<?=md5(uniqid(""));?>"> <!-- ID PARA ANALISE -->
          <input type="hidden" name="id" class="nId" value="">
          <input type="number" min='1' placeholder="Quantidade de Livros" name="quantidade"/>

          <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat"><input type="submit" value="Comprar"></a>
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Cancelar</a>
          </div>
          
        </form>
      </div>
    </div>

      <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
      <script type="text/javascript" src="js/materialize.min.js"></script>
      <script type="text/javascript" src="js/script.js"></script>
      <script type="text/javascript" src="js/typed.min.js"></script>
      <script type="text/javascript" src="js/owl.carousel.min.js"></script>

      <script type="text/javascript">
        // Gerar Indices 
        var i = null;
        const livros = [];
        $('.corpo tr').each(function(){
          var id = $(this).find('.id');
          var titulo = $(this).find('.titulo').text();

          livros.push({"titulo": titulo});
        });
        // Modal
        $(document).ready(function(){
          $('.modal').modal();
           $(".owl-carousel").owlCarousel({
            
            autoHeight:true,
            items: 2,
            loop:true,
            margin:20,
            autoplay:true,
            autoplayTimeout:4000,
            autoplayHoverPause:true,
            nav:false,
            responsive:{
                0:{
                    items:1
                }
            }
           });
        });
        // Compra parte 1
        function comprar(e) {
          $('.nId').val(e);
          $('.nProd').text(livros[e-1]['titulo']);
          $('#modal1').modal('open');
        }
        // Pesquisar ou Autocomplete
      </script>
    </body>
  </html>