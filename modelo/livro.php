<?php

class Livro {

    private $id;
    private $titulo;
    private $preco;
    private $edicao;
    private $quantidade;

    public function __construct($pid, $ptitulo, $ppreco, $pedicao, $pquantidade){
      $this->id = $pid;
      $this->titulo = $ptitulo;
      $this->preco = $ppreco;
      $this->edicao = $pedicao;
      $this->quantidade = $pquantidade;
    }


    public function getId() { return $this->id; }
    public function getTitulo() { return $this->titulo; }
    public function getPreco() { return $this->preco; }
    public function getEdicao() { return $this->edicao; }
    public function getQuantidade() { return $this->quantidade; }

}
?>
