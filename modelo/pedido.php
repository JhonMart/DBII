<?php
  include_once("item.php");

  class Pedido {
    private $id;
    private $item;
    private $user;

    public function __construct($pid, $pitem, $puser){
      $this->id = $pid;
      $this->item = $pitem;
      $this->user = $puser;
    }

    public function getId(){ return $this->id; }
    public function getItem(){ return $this->item; }
    public function getUser(){ return $this->user; }
  }

?>