<?php

  class Item {
    private $id;
    private $quantidade;

    public function __construct($pid, $pquantidade){
      $this->id = $pid;
      $this->quantidade = $pquantidade;
    }

    public function getId(){ return $this->id; }
    public function getQuantidade(){ return $this->quantidade; }

  }

?>
