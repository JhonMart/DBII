  $(document).ready(function(){
   $(".button-collapse").sideNav();
  
   var write = new Typed('.write', {
	    strings: ['Procure seu livro'],
	    typeSpeed: 40,
	    backSpeed: 30,
	    attr: 'placeholder',
    	bindInputFocusEvents: true,
	    smartBackspace: true, // this is a default
	    loop: true
  	});
   
   var $toastContent = $('<span>"Todos os seus sonhos podem se tornar realidade se voc&ecirc; tem coragem para persegu&iacute;-los"</span>').add($('<button class="btn-flat toast-action">Fechar</button>'));
  Materialize.toast($toastContent, 10000);
  });
     