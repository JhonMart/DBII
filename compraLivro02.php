 

  <!DOCTYPE html>
  <html>
    <head>
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
      <link type="text/css" rel="stylesheet" href="css/owl.carousel.min.css"  media="screen,projection"/>
      <link type="text/css" rel="stylesheet" href="css/estilo.css"  media="screen,projection"/>

      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
      <script type="text/javascript" src="https://cdn.bootcss.com/typed.js/1.1.4/typed.min.js"></script>
      <style type="text/css">
          #logo{
              width: 100px;
              transition-duration: .5s;
          }
          #logo:hover{
              filter: grayscale(1);
              transition-duration: .5s;
              margin-top:50px;
              transform: scale(3,3) rotate(-10deg);

              
          }
      </style>
      <!--Let browser know website is optimized for mobile-->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>

    <body>
    <div class="background"></div>
     <nav class="transparent z-depth-0">
    <div class="nav-wrapper container">
      <a href="#!" class="brand-logo"><img id="logo" class="responsive-img" src="images/logo2.png"></a>
      <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
      <ul class="right hide-on-med-and-down">
        <li><a href="index.php"><i class="material-icons left">home</i>Início</a></li>
        <li><a href="index1.php"><i class="material-icons left">monetization_on</i>Comprar</a></li>
        <li><a href="cadastro.php"><i class="material-icons left">library_books</i>Cadastrar Livro</a></li>
        <li><a href="sobre.php"><i class="material-icons left">info</i>Sobre</a></li>
        
      </ul>
      <ul class="side-nav" id="mobile-demo">
        <li><a href="index.php"><i class="material-icons left">home</i>Início</a></li>
        <li><a href="index1.php"><i class="material-icons left">monetization_on</i>Comprar</a></li>
        <li><a href="cadastro.php"><i class="material-icons left">library_books</i>Cadastrar Livro</a></li>
        <li><a href="sobre.php"><i class="material-icons left">info</i>Sobre</a></li>
      </ul>
    </div>
    
  </nav>
  <div class="container" style="margin-top: 90px;">

  <div class="card">
      <div class="card-content">
      <a href="index1.php" class="pulse btn btn-floating btn-large blue right"><i class="material-icons">arrow_back</i></a>
        <?php
            error_reporting(0);
            include_once("./controle/livroControle.php");
            include_once("./modelo/livro.php");
            include_once("./modelo/pedido.php");
            include_once("./modelo/item.php");
            include_once("./uteis/uteis.php");


            $id =  isset($_POST["id"]) ? $_POST["id"] : '';

            $quantidade =  isset($_POST["quantidade"]) ? $_POST["quantidade"] : '';

            $user =  isset($_POST['user']) ? $_POST['user'] : 'FAIL';

            echo "ID: " . $id . " QTD: " . $quantidade;

            $controle = new LivroControle();
            $livro = $controle->consultarLivrobyId($id);

            if($livro->getQuantidade() >= $quantidade) {
              echo "<br>Pedido em andamento! <br>";

              $item = new Item($id, $quantidade);
              $pedido = new Pedido(0, $item, $user);
              $retorno = $controle->realizarPedido($pedido);

              if( $retorno == "success"){
                echo "<br> Pedido realizado com sucesso: " . $retorno . "<br>";
              }
              else{
                echo "<br> ERRO: " . $retorno . "<br>";
              }
            }
            else {
              echo "<br>Quantidade Indisponível! <br>";
            }

            imprimirLivroDetalhado($livro);

            $controle = null;
          ?>

          <?php 
              require './controle/livroControle.php';
              $nLivro = new LivroControle();
          ?>

          <?php 
            require './controle/livroControle.php';
            $nLivro = new LivroControle();
          ?>

    </div>
  </div>
  
    </div>
  <div class="fixed-action-btn toolbar">
    <a class="btn-floating btn-large blue">
      <i class="large material-icons">mode_edit</i>
    </a>
    <ul>
      <li class="waves-effect waves-light"><a href="./cadastro.php"><i class="material-icons">library_add</i></a></li>
      <li class="waves-effect waves-light"><a href="#!"><i class="material-icons">format_quote</i></a></li>
      <li class="waves-effect waves-light"><a href="#!"><i class="material-icons">publish</i></a></li>
      <li class="waves-effect waves-light"><a href="#!"><i class="material-icons">attach_file</i></a></li>
    </ul>
  </div>
 
    

      <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
      <script type="text/javascript" src="js/materialize.min.js"></script>
      <script type="text/javascript" src="js/script.js"></script>
      <script type="text/javascript" src="js/typed.min.js"></script>
      <script type="text/javascript" src="js/owl.carousel.min.js"></script>

      <script type="text/javascript">
        // Gerar Indices 
        var i = null;
        const livros = [];
        $('.corpo tr').each(function(){
          var id = $(this).find('.id');
          var titulo = $(this).find('.titulo').text();

          livros.push({"titulo": titulo});
        });
        // Modal
        $(document).ready(function(){
          $('.modal').modal();
           $(".owl-carousel").owlCarousel({
            
            autoHeight:true,
            items: 2,
            loop:true,
            margin:20,
            autoplay:true,
            autoplayTimeout:4000,
            autoplayHoverPause:true,
            nav:false,
            responsive:{
                0:{
                    items:1
                }
            }
           });
        });
        // Compra parte 1
        function comprar(e) {
          $('.nId').val(e);
          $('.nProd').text(livros[e-1]['titulo']);
          $('#modal1').modal('open');
        }
        // Pesquisar ou Autocomplete
      </script>
    </body>
  </html>          