<?php

include_once("./dao/livroDAO.php");
include_once("./dao/pedidoDAO.php");

class LivroControle{
	// construtor
	public function __construct(){
    if(isset($_POST['cadastrar'])){
      $this->cadastrar($_POST['titulo'], $_POST['preco'], $_POST['edicao'], $_POST['quantidade']);
    }
	}

  public function consultarLivros(){
    $dao = new LivroDAO();
    $livros = $dao->consultarLivros();

    $dao = null;

    return $livros;
  }

  public function consultarLivrobyId($id){
    $dao = new LivroDAO();
    $livro = $dao->consultarLivrobyId($id);

    $dao = null;

    return $livro;
  }

	public function realizarPedido($pedido){
    $dao = new PedidoDAO();
    $retorno = $dao->inserir($pedido);

    $dao = null;

    return $retorno;
  }

  public function cadastrar($titulo, $preco, $edicao, $quantidade){
    $quantidade = (int) $quantidade;
    $preco = (float) $preco;

    $firebase = new Firebase\FirebaseLib(DEFAULT_URL, DEFAULT_TOKEN);

    $id = count(json_decode($firebase->get("/Livraria/livros/"),true));
    $livro = array(
        "id" => ($id+1),
        "titulo" => $titulo,
        "preco" => $preco,
        "edicao" => $edicao,
        "quantidade" => $quantidade
    );
    $firebase->set("/Livraria/livros/$id/", $livro);
  }
}
?>